import java.util.ArrayList;


public class StringPermutations {
	
	public static ArrayList<String> permutation(String s) {
	    ArrayList<String> result = new ArrayList<String>();
	    // If input string's length is 1, return {s}
	    if (s.length() == 1) {
	    	result.add(s);
	    } 
	    else if (s.length() > 1) {
	    	int lastIndex = s.length() - 1;
	        // Find out the last character
	        String last = s.substring(lastIndex);
	        // Rest of the string
	        String rest = s.substring(0, lastIndex);
	        // Perform permutation on the rest string and
	        // merge with the last character
	        result = merge(permutation(rest), last);
	    }
	    return result;
	}

	public static ArrayList<String> merge(ArrayList<String> list, String c) {
	    ArrayList<String> result = new ArrayList<String>();
	    // Loop through all the string in the list
	    for (String s : list) {
	        // For each string, insert the last character to all possible positions
	        for (int i = 0; i <= s.length(); ++i) {
	            String ps = new StringBuilder(s).insert(i, c).toString();
	            result.add(ps);
	        }
	    }
	    return result;
	}	
	
	
	public static void main(String[] args) {
		ArrayList<String> result = permutation( "ab");
		System.out.println(result);
		
	}
}
